import { Request, Response } from "express";
import { model } from "../model";
import { messages } from "../../../core";
import { authenticate } from "../../session/services";
import _ from "lodash";
import {sendConfirmation} from "../../emailConfirmation/services";
import {MailerType} from "../../../core/mail";
import axios from "axios";
import {cdn} from "../../../core/api";

export const update = (mailer: MailerType) => async (req: Request, res: Response) => {
    const input = req.body;
    const user = await authenticate(req);
    if (user) {
        const updates = {
            ...(input.name ? { name: input.name } : {}),
            ...(input.lastName ? { lastName: input.lastName } : {}),
            ...(input.phone ? { phone: input.phone } : {}),
            ...(input.avatar ? { avatar: input.avatar } : {}),
            ...(input.email ? { email: input.email } : {}),
            ...(input.signature ? { signature: input.signature } : {}),
        };
        if (input.avatar && user.avatar) {
            try {
                await cdn.delete(encodeURI(`remove?file=${user.avatar}`));
            } catch (err) {
                console.log(err);
            }
        }
        if (input.signature && user.signature) {
            try {
                await cdn.delete(encodeURI(`remove?file=${user.signature}`));
            } catch (err) {
                console.log(err);
            }
        }
        if (Object.keys(updates).length) {
            const checkEmail = updates.email ? await model.findOne({ email: updates.email }).exec() : null;
            if (checkEmail) {
                return res.status(405).send({
                   message: 'email exists',
                });
            } else {
                const updatedUser = await model.findByIdAndUpdate(user._id, { $set: { ...updates, ...(updates.email ? { confirmed: false } : {}) }}, { new: true });
                if (updates.email) {
                    await sendConfirmation(updatedUser, mailer);
                }
                return res.status(200).send(_.omit(updatedUser.toObject(), ['organizations', 'ownedOrganizations', '__v', 'password']));
            }
        } else {
            return res.status(200).send(user);
        }
    } else {
        return res.status(401).send({
            message: messages.Errors.UNAUTHORIZED
        });
    }
};