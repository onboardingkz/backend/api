import { Request, Response } from "express";
import { model } from "../model";
import { messages } from "../../../core";
import { authenticate } from "../../session/services";
import _ from "lodash";

export const get = async (req: Request, res: Response) => {
    const params = req.params;
    if (params?.id) {
        const user = await model.findById(params.id);
        if (user) {
            return res.status(200).send(_.omit(user.toObject(), ['organizations', 'ownedOrganizations', '__v', 'password']));
        } else {
            return res.status(404).send({
                message: messages.Errors.NOT_FOUND
            });
        }
    } else {
        return res.status(400).send({
            message: messages.Errors.WRONG_INPUT
        });
    }
};