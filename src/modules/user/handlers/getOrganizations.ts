import { Request, Response } from "express";
import { model } from "../model";
import { messages } from "../../../core";
import { authenticate } from "../../session/services";

export const getOrganizations = async (req: Request, res: Response) => {
    const input = req.body;
    const user = await authenticate(req);
    if (user) {
        const organizations = await model.findById(user._id).populate({
            path: 'organizations',
            select: '-members -__v'
        }).exec();
        return res.status(200).send(organizations.toObject().organizations || []);
    } else {
        return res.status(401).send({
            message: messages.Errors.UNAUTHORIZED
        });
    }
};