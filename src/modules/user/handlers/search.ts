import { Request, Response } from "express";
import { model } from "../model";
import _ from "lodash";

export const search = async (req: Request, res: Response) => {
    const input = req.body;
    const query = input.term ? {
        $or: ['email', 'name', 'lastName', 'phone'].map(n => (
            { [n]: { $regex: `^${input.term}`, $options: 'i' }}))
        } : {
        ...(input.email ? { email: { $regex: `^${input.email}`, $options: 'i' } } : {}),
        ...(input.name ? { name: { $regex: `^${input.name}`, $options: 'i' } } : {}),
        ...(input.lastName ? { lastName: { $regex: `^${input.lastName}`, $options: 'i' } } : {}),
        ...(input.phone ? { phone: { $regex: `^${input.phone}`, $options: 'i' } } : {}),
    };

    const users = Object.keys(query).length ? (await model.find(query)) : [];
    return res.status(200).send(users.map(n => _.omit(n.toObject(), ['organizations', 'ownedOrganizations', '__v', 'password'])));
};