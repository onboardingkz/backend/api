import { Request, Response } from "express";
import { build } from "../model";
import { build as buildSession } from "../../session/model";
import * as _ from 'lodash';
import { generateToken } from "../../../core/utils";
import { validateEmail, validatePassword } from "../../../core/validators";
import { messages } from "../../../core";
import { MailerType } from "../../../core/mail";
import { sendConfirmation } from "../../emailConfirmation/services";

export const register = (mailer: MailerType) => async (req: Request, res: Response) => {
    const input = req.body;
    if (validateEmail(input?.email) && validatePassword(input?.password)) {
        // Create user
        const user =  build({ email: input.email, password: input.password, name: input.name, lastName: input.lastName, phone: input.phone });
        user.save((err: unknown) => {
            if (err) {
                return res.status(400).send({ message: messages.Errors.ALREADY_EXISTS });
            } else {
                // Begin session
                const session = buildSession({user: user.id, token: generateToken(30)});
                session.save((errSession) => {
                    if (errSession) {
                        return res.status(500).send({ message: messages.Errors.SERVER_ERROR });
                    } else {
                        sendConfirmation({ id: user.id, email: input.email }, mailer);
                        return res.status(200).send({
                            user: _.omit(user.toObject(), ['password', '_id', '__v']),
                            session: _.pick(session.toObject(), ['token'])
                        });
                    }
                });
            }
        });
    } else {
        return res.status(400).send({
           message: messages.Errors.WRONG_INPUT
        });
    }
};