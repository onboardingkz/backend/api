import { Express } from "express";
import { handlers } from ".";
import { generateRoute } from "../../core/router";
import { MailerType } from "../../core/mail";

export const routes = (app: Express, mailer: MailerType) => {
    app.post(generateRoute('/auth/login'), handlers.login);
    app.post(generateRoute('/auth/register'), handlers.register(mailer));
    app.post(generateRoute('/auth/logout'), handlers.logout);
    app.get(generateRoute('/auth/user'), handlers.userByToken);
    app.put(generateRoute('/auth/user/update'), handlers.update(mailer));

    app.get(generateRoute('/user/organizations/owned'), handlers.getOwnedOrganizations);
    app.get(generateRoute('/user/organizations'), handlers.getOrganizations);
    app.get(generateRoute('/user/:id'), handlers.get);
    app.post(generateRoute('/users/search'), handlers.search);
}