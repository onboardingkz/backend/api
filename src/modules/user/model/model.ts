import mongoose from "mongoose";

const schema = new mongoose.Schema({
    email: {
        type: String,
        required: true,
        unique: true
    },
    name: {
        type: String,
        default: null
    },
    lastName: {
        type: String,
        default: null
    },
    avatar: {
        type: String,
        default: null
    },
    phone: {
        type: String,
        default: null
    },
    position: {
        type: String,
        default: null
    },
    password: {
        type: String,
        required: true
    },
    confirmed: {
        type: Boolean,
        default: false
    },
    signature: {
        type: String
    },
    organizations: {
        type: [mongoose.SchemaTypes.ObjectId],
        ref: "Organization",
    },
    ownedOrganizations: {
        type: [mongoose.SchemaTypes.ObjectId],
        ref: "Organization",
    },
    departments: {
        type: [mongoose.SchemaTypes.ObjectId],
        ref: "Department",
    },
    instruments: {
        type: [mongoose.SchemaTypes.ObjectId],
        ref: "Instrument",
    },
    notifications: {
        type: [mongoose.SchemaTypes.ObjectId],
        ref: "Notification",
    },
    signedDocuments: {
        type: [mongoose.SchemaTypes.ObjectId],
        ref: "SignedDocument"
    }
});

export const model = mongoose.model('User', schema);

