import mongoose from "mongoose";

export type Resource = {
    readonly id?: mongoose.Types.ObjectId,
    readonly email: string;
    readonly name?: string;
    readonly lastName?: string;
    readonly phone?: string;
    readonly position?: string;
    readonly avatar?: string;
    readonly signature?: string;
    readonly password: string;
    readonly organizations?: ReadonlyArray<mongoose.Schema.Types.ObjectId>;
    readonly ownedOrganizations?: ReadonlyArray<mongoose.Schema.Types.ObjectId>;
    readonly confirmed?: boolean;
    readonly notifications?: ReadonlyArray<mongoose.Schema.Types.ObjectId>;
    readonly instruments?: ReadonlyArray<mongoose.Schema.Types.ObjectId>;
    readonly signedDocuments?: ReadonlyArray<mongoose.Schema.Types.ObjectId>;
}
