import mongoose from "mongoose";
import {middlewares} from "./middlewares";

const schema = new mongoose.Schema({
    title: {
        type: String,
        required: true,
    },
    parent: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: "Department",
        required: false,
    },
    organization: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: "Organization",
    },
    children: {
        type: [mongoose.SchemaTypes.ObjectId],
        ref: "Department"
    },
    members: {
        type: [mongoose.SchemaTypes.ObjectId],
        ref: "User"
    }
});

middlewares(schema);

export const model = mongoose.model('Department', schema);

