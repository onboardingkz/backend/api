import mongoose from "mongoose";

export type Resource = {
    readonly title: string;
    readonly parent?: mongoose.Schema.Types.ObjectId;
    readonly organization: mongoose.Schema.Types.ObjectId;
    readonly children?: ReadonlyArray<mongoose.Schema.Types.ObjectId>;
    readonly members?: ReadonlyArray<mongoose.Schema.Types.ObjectId>;
}
