import * as mongoose from "mongoose";

export const middlewares = (schema: mongoose.Schema) => {
    schema.pre('remove', { query: true, document: true }, async function(next) {
        if (this.parent) {
            await this.model('Department').findByIdAndUpdate(this.parent, { $pull: { children: this._id }});
        }
        await this.model('Organization').findByIdAndUpdate(this.organization, { $pull: { departments: this._id }});
        await this.model('User').updateMany({ _id: { $in: this.members }}, { $pull: { departments: this._id }});
        next();
    });
}