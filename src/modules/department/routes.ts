import { Express } from "express";
import { handlers } from ".";
import { generateRoute } from "../../core/router";

export const routes = (app: Express) => {
    app.post(generateRoute('/department'), handlers.create);
    app.get(generateRoute('/department/:id'), handlers.read);
    app.put(generateRoute('/department/:id'), handlers.update);
    app.delete(generateRoute('/department/:id'), handlers.remove);

    app.post(generateRoute('/department/:id/member'), handlers.addMember);
    app.delete(generateRoute('/department/:id/member'), handlers.removeMember);
    app.get(generateRoute('/department/:id/members'), handlers.getMembers);
}