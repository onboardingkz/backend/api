import { Request, Response } from "express";
import { model } from "../model";
import { messages } from "../../../core";
import { authenticate } from "../../session/services";
import _ from "lodash";
import {getIfOwn} from "../services";
import {checkObjectId} from "../../../core/database";

export const update = async (req: Request, res: Response) => {
    const input = req.body;
    const params = req.params;
    const user = await authenticate(req);
    const department = await getIfOwn(user, params.id);
    if (user && department) {
        let query: Record<string, string> = {
            ...(input.title ? { title: input.title } : {}),
        };

        if (Object.keys(input).includes('parentId')) {
            if (checkObjectId(input?.parentId) && await getIfOwn(user, input.parentId)) {
                query = {
                    ...query,
                    parent: input?.parentId
                };
            } else if (input.parentId === null) {
                query = {
                    ...query,
                    parent: null
                }
            }
            else {
                return res.status(400).send({
                    message: messages.Errors.NOT_FOUND
                });
            }
        }

        if (query.parent) {
            await model.findByIdAndUpdate(department.parent, { $pull: { children: department.id }});
            await model.findByIdAndUpdate(query.parent, { $addToSet: { children: department.id }});
        }
        const updatedDepartment = await model.findByIdAndUpdate(department.id, { $set: query}, { new: true });
        return res.status(200).send(_.omit(updatedDepartment.toObject(), []));
    } else {
        return res.status(401).send({
            message: messages.Errors.UNAUTHORIZED
        });
    }
};