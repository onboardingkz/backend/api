import { Request, Response } from "express";
import { model} from "../model";
import { messages } from "../../../core";
import { authenticate } from "../../session/services";
import _ from "lodash";
import { getIfOwn } from "../services";
import { selectors as selectorsUser } from '../../user';
import { selectors } from "..";

export const read = async (req: Request, res: Response) => {
    const params = req.params;
    const user = await authenticate(req);
    const department = await getIfOwn(user, params.id);
    if (user && department) {
        const updatedDepartment = await model.findById(department.id).populate(selectors.populator, selectorsUser.secure);
        return res.status(200).send(_.omit(updatedDepartment.toObject(), []));
    } else {
        return res.status(401).send({
            message: messages.Errors.UNAUTHORIZED
        });
    }
};