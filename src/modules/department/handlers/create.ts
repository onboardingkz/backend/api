import { Request, Response } from "express";
import {build, model, Resource} from "../model";
import { messages } from "../../../core";
import { authenticate } from "../../session/services";
import { getIfOwn } from "../../organization/services";
import { checkObjectId } from "../../../core/database";
import { model as modelOrganization } from '../../organization/model';

export const create = async (req: Request, res: Response) => {
    const input = req.body;
    const user = await authenticate(req);
    const organization = await getIfOwn(user, req);
    if (user && organization) {
        if (input?.title) {
            const payload: Resource = { title: input.title, organization: organization.id, parent: null };
            let department = build(payload);

            // If parent defined
            if (input?.parentId) {
                if (checkObjectId(input?.parentId) && await model.findById(input?.parentId)) {
                    const payloadWithParent = {
                        ...payload,
                        parent: input?.parentId
                    };
                    department = build(payloadWithParent);
                } else {
                    return res.status(400).send({
                        message: messages.Errors.NOT_FOUND
                    });
                }
            }

            department.save(async (err) => {
                if (err) {
                    return res.status(500).send({ message: messages.Errors.SERVER_ERROR });
                } else {
                    await modelOrganization.findByIdAndUpdate(organization._id, { $addToSet: { departments: department.id } });
                    if (input.parentId) {
                        await model.findByIdAndUpdate(input.parentId, { $addToSet: { children: department.id }});
                    }
                    return res.status(200).send(department);
                }
            });
        } else {
            return res.status(400).send({
                message: messages.Errors.WRONG_INPUT
            });
        }
    } else {
        return res.status(401).send({
            message: messages.Errors.UNAUTHORIZED
        });
    }
};