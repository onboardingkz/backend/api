import { Request, Response } from "express";
import { messages } from "../../../core";
import { authenticate } from "../../session/services";
import { getIfOwn } from "../services";

export const remove = async (req: Request, res: Response) => {
    const params = req.params;
    const user = await authenticate(req);
    const department = await getIfOwn(user, params.id);
    if (user && department) {
        await department.remove();
        return res.status(200).send({
            message: messages.Success.Default
        });
    } else {
        return res.status(401).send({
            message: messages.Errors.UNAUTHORIZED
        });
    }
};