import { Request, Response } from "express";
import { model as modelUser } from "../../../user/model";
import { messages } from "../../../../core";
import { authenticate } from "../../../session/services";
import { getIfOwn } from "../../services";
import { checkObjectId } from "../../../../core/database";
import { addMemberToDepartment } from "../../../shared/services";

export const addMember = async (req: Request, res: Response) => {
    const input = req.body;
    const params = req.params;
    const user = await authenticate(req);
    const department = await getIfOwn(user, params.id);
    if (user && department) {
        if (checkObjectId(input.userId)) {
            if (await modelUser.findById(input.userId)) {
                await addMemberToDepartment(params.id, input.userId);
                return res.status(200).send(department);
            } else {
                return res.status(400).send({
                   message: messages.Errors.NOT_FOUND
                });
            }
        } else {
            return res.status(400).send({
                message: messages.Errors.WRONG_INPUT
            });
        }
    } else {
        return res.status(401).send({
            message: messages.Errors.UNAUTHORIZED
        });
    }
};