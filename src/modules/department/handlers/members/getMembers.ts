import { Request, Response } from "express";
import { messages } from "../../../../core";
import { authenticate } from "../../../session/services";
import { getIfOwn } from "../../services";

export const getMembers = async (req: Request, res: Response) => {
    const params = req.params;
    const user = await authenticate(req);
    const department = await getIfOwn(user, params.id);
    if (user && department) {
        return res.status(200).send(department.toObject().members || []);
    } else {
        return res.status(401).send({
            message: messages.Errors.UNAUTHORIZED
        });
    }
};