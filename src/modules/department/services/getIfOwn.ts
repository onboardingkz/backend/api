import { model } from "../model";
import { checkObjectId } from "../../../core/database";
import { Resource as UserResource } from "../../user/model";
import { selectors } from "..";

export const getIfOwn = async (user: UserResource, id: string) => {
    if (checkObjectId(id)) {
        const department = await model.findById(id).populate(selectors.populator);
        return user?.ownedOrganizations?.includes(department?.organization) ? department : null;
    } else return null;
}