import { Express } from "express";
import { handlers } from ".";
import { generateRoute } from "../../core/router";
import {Socket} from "socket.io-client";

export const routes = (app: Express, socket: Socket) => {
    app.get(generateRoute('/notifications/all'), handlers.all(socket));
    // app.post(generateRoute('/notification'), handlers.create(socket));
    app.post(generateRoute('/notifications/opened'), handlers.opened(socket));
}