import { Request, Response } from "express";
import {build, model} from "../model";
import { model as modelUser } from '../../user/model';
import { messages } from "../../../core";
import { authenticate } from "../../session/services";
import _ from "lodash";
import {Socket} from "socket.io-client";

export const create = (socket: Socket) => async (req: Request, res: Response) => {
    const input = req.body;
    const user = await authenticate(req);
    if (user) {
        if (input?.title && input?.category) {
            const notification = build({ title: input.title, category: input.category, user: user.id });
            notification.save(async (err) => {
                if (err) {
                    return res.status(500).send({
                        message: messages.Errors.SERVER_ERROR
                    });
                } else {
                    await modelUser.findByIdAndUpdate(user.id, { $addToSet: { notifications: notification._id } });
                    socket.emit('NOTIFY', { userId: user.id, notification });
                    return res.status(200).send(_.omit(notification.toObject()));
                }
            });
        } else {
            return res.status(400).send({
                message: messages.Errors.WRONG_INPUT
            });
        }
    } else {
        return res.status(401).send({
            message: messages.Errors.UNAUTHORIZED
        });
    }
};