import {Request, Response} from "express";
import {authenticate} from "../../session/services";
import {messages} from "../../../core";
import {Socket} from "socket.io-client";
import {model} from "../model";

export const opened = (socket: Socket) => async (req: Request, res: Response) => {
    const input = req.body;
    const user = await authenticate(req);
    if (user) {
        await model.updateMany({ user: user.id, read: false }, { $set: { read: true } }).exec();
        socket.emit('CLEAR', { userId: user.id });

        const notifications = await model.find({ user: user.id }, null, { sort: { createdAt: -1 } }).exec();

        return res.status(200).send(notifications);
    } else {
        return res.status(401).send({
            message: messages.Errors.UNAUTHORIZED
        });
    }
};