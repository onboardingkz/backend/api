import mongoose from "mongoose";

const schema = new mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    category: {
        type: String,
    },
    read: {
        type: Boolean,
        default: false
    },
    user: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: "User",
    },
}, {
    timestamps: true
});

export const model = mongoose.model('Notification', schema);

