import * as mongoose from "mongoose";

export type Resource = {
    readonly title: string;
    readonly category: string;
    readonly read?: boolean;
    readonly user: mongoose.Schema.Types.ObjectId;
}
