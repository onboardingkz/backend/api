import { model } from "../model";
import { checkObjectId } from "../../../core/database";
import { Resource as UserResource } from "../../user/model";

export const getIfOwn = async (user: UserResource, id: string) => {
    if (checkObjectId(id)) {
        const document = await model.findById(id);
        return user?.ownedOrganizations?.includes(document?.organization) ? document : null;
    } else return null;
}