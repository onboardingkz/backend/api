import { Request, Response } from "express";
import { model } from "../model";
import { messages } from "../../../core";
import { authenticate } from "../../session/services";
import { getIfOwn } from "../services";

export const update = async (req: Request, res: Response) => {
    const input = req.body;
    const params = req.params;
    const user = await authenticate(req);
    const document = await getIfOwn(user, params.id);
    if (user && document) {
        const query = {
            ...(input.title ? { title: input.title } : {}),
            ...(input.file ? { file: input.file } : {}),
            ...(input.signaturePosition ? { signaturePosition: input.signaturePosition } : {})
        };
        const updatedDocument = await model.findByIdAndUpdate(params.id, { $set: query }, { new: true });
        return res.status(200).send(updatedDocument);
    } else {
        return res.status(401).send({
            message: messages.Errors.UNAUTHORIZED
        });
    }
};