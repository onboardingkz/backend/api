import { Express } from "express";
import { handlers } from ".";
import { generateRoute } from "../../core/router";

export const routes = (app: Express) => {
    app.post(generateRoute('/document'), handlers.create);
    app.delete(generateRoute('/document/:id'), handlers.remove);
    app.put(generateRoute('/document/:id'), handlers.update);
}