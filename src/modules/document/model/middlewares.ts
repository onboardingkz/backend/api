import * as mongoose from "mongoose";
import {cdn} from "../../../core/api";

export const middlewares = (schema: mongoose.Schema) => {
    schema.pre('remove', { query: true, document: true }, async function() {
        if (this.file) {
            try {
                await cdn.delete(encodeURI(`/remove?file=${this.file}`));
            } catch (e) {
                console.log(e);
            }
        }
        await Promise.all(this.signedDocuments.map(async (document: string) => {
            const doc = await this.model('SignedDocument').findById(document);
            doc.remove();
        }));
        await this.model('Organization').findByIdAndUpdate(this.organization, { $pull: { documents: this._id }});
    });
}