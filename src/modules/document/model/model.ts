import mongoose from "mongoose";
import { middlewares } from "./middlewares";

const schema = new mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    file: {
        type: String,
        required: true
    },
    signaturePosition: [{
        x: { type: "Number" },
        y: { type: "Number" },
        page: { type: "Number" }
    }],
    organization: {
        type: mongoose.Types.ObjectId,
        ref: "Organization",
        required: true
    },
    signedDocuments: {
        type: [mongoose.Types.ObjectId],
        ref: "SignedDocument"
    }
});

middlewares(schema);

export const model = mongoose.model('Document', schema);

