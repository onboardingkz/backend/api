import * as mongoose from "mongoose";

export type Resource = {
    readonly title: string;
    readonly file: string;
    readonly signaturePosition: ReadonlyArray<{
        x: number;
        y: number;
        page: number;
    }>;
    readonly organization: mongoose.Types.ObjectId;
    readonly signedDocuments?: ReadonlyArray<mongoose.Types.ObjectId>;
}
