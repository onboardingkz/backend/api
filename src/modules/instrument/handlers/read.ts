import { Request, Response } from "express";
import { messages } from "../../../core";
import { authenticate } from "../../session/services";
import { getIfMember } from "../services";

export const read = async (req: Request, res: Response) => {
    const params = req.params;
    const user = await authenticate(req);
    const instrument = await getIfMember(user, params.id);
    if (user) {
        if (instrument) {
            return res.status(200).send(instrument);
        } else {
            return res.status(400).send({
                message: messages.Errors.NOT_FOUND
            });
        }
    } else {
        return res.status(401).send({
            message: messages.Errors.UNAUTHORIZED
        });
    }
};