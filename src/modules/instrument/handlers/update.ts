import { Request, Response } from "express";
import { model } from "../model";
import { messages } from "../../../core";
import { authenticate } from "../../session/services";
import { getIfOwn } from "../services";

export const update = async (req: Request, res: Response) => {
    const input = req.body;
    const params = req.params;
    const user = await authenticate(req);
    const instrument = await getIfOwn(user, params.id);
    if (user && instrument) {
        const query = {
            ...(input.title ? { title: input.title } : {}),
            ...(input.description ? { description: input.description } : {}),
            ...(input.image ? { image: input.image } : {}),
        }
        const updatedInstrument = await model.findByIdAndUpdate(params.id, { $set: query}, { new: true });
        return res.status(200).send(updatedInstrument);
    } else {
        return res.status(401).send({
            message: messages.Errors.UNAUTHORIZED
        });
    }
};