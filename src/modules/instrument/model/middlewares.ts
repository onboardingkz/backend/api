import * as mongoose from "mongoose";

export const middlewares = (schema: mongoose.Schema) => {
    schema.pre('remove', { query: true, document: true }, async function() {
        await this.model('Organization').findByIdAndUpdate(this.organization, { $pull: { instruments: this._id }});
    });
}