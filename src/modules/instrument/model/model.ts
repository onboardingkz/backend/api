import mongoose from "mongoose";
import { middlewares } from "./middlewares";

const schema = new mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: false,
        default: null
    },
    image: {
        type: String,
        required: false,
        default: null
    },
    members: {
        type: [mongoose.Types.ObjectId],
        ref: "User",
    },
    accessRequests: {
        type: [mongoose.Types.ObjectId],
        ref: "AccessRequest",
    },
    organization: {
        type: mongoose.Types.ObjectId,
        ref: "Organization",
        required: true
    },
});

middlewares(schema);

export const model = mongoose.model('Instrument', schema);

