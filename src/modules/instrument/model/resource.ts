import * as mongoose from "mongoose";

export type Resource = {
    readonly title: string;
    readonly description?: string;
    readonly image?: string;
    readonly members?: ReadonlyArray<mongoose.Types.ObjectId>;
    readonly accessRequests?: ReadonlyArray<mongoose.Types.ObjectId>;
    readonly organization: mongoose.Types.ObjectId;
}
