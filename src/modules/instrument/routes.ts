import { Express } from "express";
import { handlers } from ".";
import { generateRoute } from "../../core/router";

export const routes = (app: Express) => {
    app.post(generateRoute('/instrument'), handlers.create);
    app.delete(generateRoute('/instrument/:id'), handlers.remove);
    app.put(generateRoute('/instrument/:id'), handlers.update);
    app.get(generateRoute('/instrument/:id'), handlers.read);
}