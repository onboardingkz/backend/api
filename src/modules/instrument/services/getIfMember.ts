import { model } from "../model";
import { checkObjectId } from "../../../core/database";
import { Resource as UserResource } from "../../user/model";

export const getIfMember = async (user: UserResource, id: string) => {
    if (checkObjectId(id)) {
        const instrument = await model.findById(id);
        return user?.organizations?.includes(instrument?.organization) ? instrument : null;
    } else return null;
}