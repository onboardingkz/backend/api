import { model } from "../model";
import { checkObjectId } from "../../../core/database";
import { Resource as UserResource } from "../../user/model";

export const getIfOwn = async (user: UserResource, id: string) => {
    if (checkObjectId(id)) {
        const accessRequest = await model.findById(id).populate('instrument');
        return user?.ownedOrganizations?.includes(accessRequest?.instrument.organization) ? accessRequest : null;
    } else return null;
}