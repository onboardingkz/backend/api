import { Request, Response } from "express";
import { model } from "../model";
import { messages } from "../../../core";
import { authenticate } from "../../session/services";
import { getIfOwn } from "../services";

export const read = async (req: Request, res: Response) => {
    const params = req.params;
    const user = await authenticate(req);
    const accessRequest = await getIfOwn(user, params.id);
    if (user) {
        if (accessRequest) {
            return res.status(200).send(accessRequest);
        } else {
            return res.status(400).send({
                message: messages.Errors.NOT_FOUND
            });
        }
    } else {
        return res.status(401).send({
            message: messages.Errors.UNAUTHORIZED
        });
    }
};