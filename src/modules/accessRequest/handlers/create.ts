import { Request, Response } from "express";
import { build } from "../model";
import { messages } from "../../../core";
import { authenticate } from "../../session/services";
import { model as modelInstrument } from "../../instrument/model";
import { Socket } from "socket.io-client";
import { build as buildNotification } from "../../notification/model";
import { model as modelUser } from "../../user/model";
import { model as modelOrganization } from "../../organization/model";

export const create = (socket: Socket) => async (req: Request, res: Response) => {
    const input = req.body;
    const user = await authenticate(req);
    if (user) {
        if (await modelInstrument.findById(input.instrumentId)) {
            const accessRequest = build({ instrument: input.instrumentId, from: user.id });
            accessRequest.save(async (err) => {
                if (err) {
                    return res.status(500).send({
                        message: messages.Errors.SERVER_ERROR
                    });
                } else {
                    const instrument = await modelInstrument.findByIdAndUpdate(input.instrumentId, { 
                        $addToSet: { accessRequests: accessRequest._id } }, 
                        { new: true }
                    );
                    const organization = await modelOrganization.findById(instrument.organization);
                    const notification = buildNotification({ 
                        title: `[${organization.name}] New access request for ${instrument.title}`, 
                        category: 'access_request', 
                        user: organization.admin
                    });
                    notification.save(async (err) => {
                        if (err) {
                            return res.status(500).send({
                                message: messages.Errors.SERVER_ERROR
                            });
                        } else {
                            await modelUser.findByIdAndUpdate(organization.admin, 
                                { $addToSet: { notifications: notification._id } }
                            );
                            socket.emit('NOTIFY', { userId: organization.admin, notification });
                            return res.status(200).send(accessRequest);
                        }
                    });
                }
            });
        } else {
            return res.status(400).send({
                message: messages.Errors.WRONG_INPUT
            });
        }
    } else {
        return res.status(401).send({
            message: messages.Errors.UNAUTHORIZED
        });
    }
};
