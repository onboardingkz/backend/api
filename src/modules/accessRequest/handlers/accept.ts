import { Request, Response } from "express";
import { messages } from "../../../core";
import { authenticate } from "../../session/services";
import { getIfOwn } from "../services";
import { Socket } from "socket.io-client";
import { build as buildNotification } from '../../notification/model';
import { model as modelUser } from "../../user/model";
import { model as modelInstrument } from "../../instrument/model";
import _ from "lodash";

export const accept = (socket: Socket) => async (req: Request, res: Response) => {
    const params = req.params;
    const user = await authenticate(req);
    const accessRequest = await getIfOwn(user, params.id);
    if (user && accessRequest) {
        await modelInstrument.findByIdAndUpdate(accessRequest.instrument.id, { $addToSet: { members: accessRequest.from }});
        await modelUser.findByIdAndUpdate(accessRequest.from, { $addToSet: { instruments: accessRequest.instrument }});
        accessRequest.remove();
        const notification = buildNotification({ title: `Your request for access to instrument ${accessRequest.instrument.title} was accepted`, category: 'access_request', user: accessRequest.from });
        notification.save(async (err) => {
            if (err) {
                return res.status(500).send({
                    message: messages.Errors.SERVER_ERROR
                });
            } else {
                await modelUser.findByIdAndUpdate(accessRequest.from, { $addToSet: { notifications: notification._id } });
                socket.emit('NOTIFY', { userId: accessRequest.from, notification });
                return res.status(200).send(_.omit(notification.toObject()));
            }
        });
    } else {
        return res.status(401).send({
            message: messages.Errors.UNAUTHORIZED
        });
    }
};