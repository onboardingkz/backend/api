import { Express } from "express";
import { handlers } from ".";
import { generateRoute } from "../../core/router";
import {Socket} from "socket.io-client";

export const routes = (app: Express, socket: Socket) => {
    app.post(generateRoute('/accessRequest'), handlers.create(socket));
    app.post(generateRoute('/accessRequest/:id'), handlers.accept(socket));
    app.delete(generateRoute('/accessRequest/:id'), handlers.decline(socket));
    app.get(generateRoute('/accessRequest/:id'), handlers.read);
}