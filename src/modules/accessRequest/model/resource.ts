import * as mongoose from "mongoose";

export type Resource = {
    readonly instrument: mongoose.Types.ObjectId;
    readonly from: mongoose.Types.ObjectId;
}
