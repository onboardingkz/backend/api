import * as mongoose from "mongoose";

export const middlewares = (schema: mongoose.Schema) => {
    schema.pre('remove', { query: true, document: true }, async function() {
        await this.model('Instrument').findByIdAndUpdate(this.instrument, { $pull: { accessRequests: this._id }});
    });
}