import mongoose from "mongoose";
import { middlewares } from "./middlewares";

const schema = new mongoose.Schema({
    instrument: {
        type: mongoose.Types.ObjectId,
        ref: "Instrument",
        required: true
    },
    from: {
        type: mongoose.Types.ObjectId,
        ref: "User",
        required: true
    },
});

middlewares(schema);

export const model = mongoose.model('AccessRequest', schema);

