import * as mongoose from "mongoose";

export type Resource = {
    readonly file: string;
    readonly organization: mongoose.Types.ObjectId;
    readonly user: mongoose.Types.ObjectId;
    readonly document: mongoose.Types.ObjectId;
}
