import * as mongoose from "mongoose";
import {cdn} from "../../../core/api";

export const middlewares = (schema: mongoose.Schema) => {
    schema.pre('remove', { query: true, document: true }, async function() {
        if (this.file) {
            try {
                await cdn.delete(encodeURI(`/remove?file=${this.file}`));
            } catch (e) {
                console.log(e);
            }
        }
        await this.model('Organization').findByIdAndUpdate(this.organization, { $pull: { signedDocuments: this._id }});
        await this.model('Document').findByIdAndUpdate(this.document, { $pull: { signedDocuments: this._id }});
        await this.model('User').findByIdAndUpdate(this.user, { $pull: { signedDocuments: this._id }});
    });
}