import mongoose from "mongoose";
import { middlewares } from "./middlewares";

const schema = new mongoose.Schema({
    file: {
        type: String,
        required: true
    },
    document: {
        type: mongoose.Types.ObjectId,
        ref: "Document",
        required: true
    },
    user: {
        type: mongoose.Types.ObjectId,
        ref: "User",
        required: true
    },
    organization: {
        type: mongoose.Types.ObjectId,
        ref: "Organization",
        required: true
    },
});

middlewares(schema);

export const model = mongoose.model('SignedDocument', schema);

