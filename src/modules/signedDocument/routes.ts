import { Express } from "express";
import { handlers } from ".";
import { generateRoute } from "../../core/router";

export const routes = (app: Express) => {
    app.post(generateRoute('/signedDocument'), handlers.create);
    app.delete(generateRoute('/signedDocument/:id'), handlers.remove);
}