import { Request, Response } from "express";
import { build } from "../model";
import { messages } from "../../../core";
import { authenticate } from "../../session/services";
import {getIfMember} from "../../organization/services";
import { model as modelOrganization } from "../../organization/model";
import { model as modelDocument } from "../../document/model";
import { model as modelUser } from "../../user/model";
import {checkObjectId} from "../../../core/database";

export const create = async (req: Request, res: Response) => {
    const input = req.body;
    const user = await authenticate(req);
    const organization = await getIfMember(user, req);
    if (user && organization) {
        if (input?.file && checkObjectId(input?.documentId)) {
            if (await modelDocument.findById(input?.documentId)) {
                const document = build({ file: input.file, organization: organization._id, document: input.documentId, user: user._id });
                document.save(async (err) => {
                    if (err) {
                        return res.status(500).send({
                            message: messages.Errors.SERVER_ERROR
                        });
                    } else {
                        await modelOrganization.findByIdAndUpdate(organization._id, { $addToSet: { signedDocuments: document._id } });
                        await modelDocument.findByIdAndUpdate(input?.documentId, { $addToSet: { signedDocuments: document._id } });
                        await modelUser.findByIdAndUpdate(user._id, { $addToSet: { signedDocuments: document._id }});
                        return res.status(200).send(document);
                    }
                });
            } else {
                return res.status(400).send({
                    message: messages.Errors.NOT_FOUND
                });
            }
        } else {
            return res.status(400).send({
                message: messages.Errors.WRONG_INPUT
            });
        }
    } else {
        return res.status(401).send({
            message: messages.Errors.UNAUTHORIZED
        });
    }
};