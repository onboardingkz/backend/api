import { Express } from "express";
import { handlers } from ".";
import { generateRoute } from "../../core/router";

export const routes = (app: Express) => {
    app.post(generateRoute('/organization'), handlers.create);
    app.get(generateRoute('/organization/:id'), handlers.read);
    app.delete(generateRoute('/organization/:id'), handlers.remove);
    app.put(generateRoute('/organization/:id'), handlers.update);
    app.post(generateRoute('/organization/:id/transfer'), handlers.transferOwnership);

    app.get(generateRoute('/organization/:id/members'), handlers.getMembers);
    app.post(generateRoute('/organization/:id/members'), handlers.addMember);
    app.delete(generateRoute('/organization/:id/members'), handlers.removeMember);

    app.get(generateRoute('/organization/:id/departments'), handlers.getTree);

    app.get(generateRoute('/organization/:id/rules'), handlers.getRules);
    app.get(generateRoute('/organization/:id/documents'), handlers.getDocuments);
    app.get(generateRoute('/organization/:id/signedDocuments'), handlers.getSignedDocuments);

    app.get(generateRoute('/organization/:id/instruments'), handlers.getInstruments);
    app.get(generateRoute('/organization/:id/accessRequests'), handlers.getAccessRequests);

}