import * as mongoose from "mongoose";

export const middlewares = (schema: mongoose.Schema) => {
    schema.post('save', { query: true, document: true }, async function() {
        await this.model('User').findByIdAndUpdate(this.admin, { $addToSet: { ownedOrganizations: this._id, organizations: this._id }});
    });

    schema.pre('remove', { query: true, document: true }, async function(next) {
        // Remove all the assignment docs that reference the removed person.
        await Promise.all(this.departments.map(async (department: string) => {
            const doc = await this.model('Department').findById(department);
            doc.remove();
        }));
        // Delete rules
        await Promise.all(this.rules.map(async (rule: string) => {
            const doc = await this.model('Rule').findById(rule);
            doc.remove();
        }));
        // Delete instruments
        await Promise.all(this.instruments.map(async (rule: string) => {
            const doc = await this.model('Instrument').findById(rule);
            doc.remove();
        }));
        // Delete documents
        await Promise.all(this.documents.map(async (rule: string) => {
            const doc = await this.model('Document').findById(rule);
            doc.remove();
        }));
        // Delete signed documents
        await Promise.all(this.documents.map(async (rule: string) => {
            const doc = await this.model('SignedDocument').findById(rule);
            doc.remove();
        }));
        // Unbound members
        await this.model('User').updateMany({ _id: { $in: this.members } }, { $pull: { organizations: this._id }});
        // Delete ownership
        await this.model('User').findByIdAndUpdate(this.admin, { $pull: { ownedOrganizations: this._id, organizations: this._id }});
        next();
    });
}