import * as mongoose from "mongoose";

export type Resource = {
    readonly name: string;
    readonly avatar?: string;
    readonly bin: number;
    readonly address?: string;
    readonly admin: mongoose.Schema.Types.ObjectId;
    readonly email?: string;
    readonly phone?: string;
    readonly members?: ReadonlyArray<mongoose.Schema.Types.ObjectId>;
    readonly rules?: ReadonlyArray<mongoose.Schema.Types.ObjectId>;
    readonly documents?: ReadonlyArray<mongoose.Schema.Types.ObjectId>;
    readonly signedDocuments?: ReadonlyArray<mongoose.Schema.Types.ObjectId>;
    readonly instruments?: ReadonlyArray<mongoose.Schema.Types.ObjectId>;
    readonly departments?: ReadonlyArray<mongoose.Schema.Types.ObjectId>;
}
