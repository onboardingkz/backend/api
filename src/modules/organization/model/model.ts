import mongoose from "mongoose";
import {middlewares} from "./middlewares";

const schema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    avatar: {
        type: String,
        default: null
    },
    bin: {
        type: String,
        required: true
    },
    admin: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: "User",
        required: true
    },
    address: {
        type: String,
        default: null
    },
    phone: {
        type: String,
        default: null
    },
    email: {
        type: String,
        default: null
    },
    members: {
        type: [mongoose.SchemaTypes.ObjectId],
        ref: "User",
    },
    rules: {
        type: [mongoose.SchemaTypes.ObjectId],
        ref: "Rule",
    },
    documents: {
        type: [mongoose.SchemaTypes.ObjectId],
        ref: "Document",
    },
    signedDocuments: {
        type: [mongoose.SchemaTypes.ObjectId],
        ref: "SignedDocument",
    },
    instruments: {
        type: [mongoose.SchemaTypes.ObjectId],
        ref: "Instrument",
    },
    departments: {
        type: [mongoose.SchemaTypes.ObjectId],
        ref: "Department",
    },
});

middlewares(schema);

export const model = mongoose.model('Organization', schema);

