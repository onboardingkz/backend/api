import { Request, Response } from "express";
import { messages } from "../../../../core";
import { authenticate } from "../../../session/services";
import { getIfMember } from "../../services";
import { model as modelSignedDocument } from '../../../signedDocument/model';

export const getSignedDocuments = async (req: Request, res: Response) => {
    const params = req.params;
    const user = await authenticate(req);
    const organization = await getIfMember(user, null, params.id);
    if (user && organization) {
        const documents = await modelSignedDocument.find({ organization: organization._id }).populate(['user', 'document']);
        return res.status(200).send(documents || []);
    } else {
        return res.status(401).send({
            message: messages.Errors.UNAUTHORIZED
        });
    }
};