import { Request, Response } from "express";
import { messages } from "../../../core";
import { authenticate } from "../../session/services";
import {getIfOwn} from "../services";

export const remove = async (req: Request, res: Response) => {
    const params = req.params;
    const user = await authenticate(req);
    const organization = await getIfOwn(user, null, params.id);
    if (user && organization) {
        await organization.remove();
        return res.status(200).send({
            message: messages.Success.Default
        });
    } else {
        return res.status(401).send({
            message: messages.Errors.UNAUTHORIZED
        });
    }
};