import { Request, Response } from "express";
import { messages } from "../../../../core";
import { model as modelUser } from "../../../user/model";
import { authenticate } from "../../../session/services";
import { getIfOwn } from "../../services";
import { validateEmail } from "../../../../core/validators";
import {removeMemberFromOrganization} from "../../../shared/services";
import {checkObjectId} from "../../../../core/database";

export const removeMember = async (req: Request, res: Response) => {
    const input = req.body;
    const params = req.params;
    const user = await authenticate(req);
    const organization = await getIfOwn(user, null, params.id);
    if (user && organization) {
        if (checkObjectId(input.userId)) {
            const member = await modelUser.findById(input.userId);

            if (member) {
                await removeMemberFromOrganization(organization.id, member.id);
                return res.status(200).send({
                    message: messages.Success.Default
                });
            } else {
                return res.status(400).send({
                    message: messages.Errors.WRONG_INPUT
                });
            }
        } else {
            return res.status(400).send({
                message: messages.Errors.WRONG_INPUT
            });
        }
    } else {
        return res.status(401).send({
            message: messages.Errors.UNAUTHORIZED
        });
    }
};