import { Request, Response } from "express";
import { messages } from "../../../../core";
import { authenticate } from "../../../session/services";
import { getIfOwn } from "../../services";
import { model as modelInstrument } from '../../../instrument/model';
import _ from 'lodash';

export const getAccessRequests = async (req: Request, res: Response) => {
    const params = req.params;
    const user = await authenticate(req);
    const organization = await getIfOwn(user, null, params.id);
    if (user && organization) {
        return res.status(200).send(_.flatten(
            await Promise.all(organization.instruments.map(async (n: { id: string; }) => {
                const instrument = await modelInstrument.findById(n.id).populate('accessRequests');
                return instrument.toObject().accessRequests;
            }))
        ) || []);
    } else {
        return res.status(401).send({
            message: messages.Errors.UNAUTHORIZED
        });
    }
};