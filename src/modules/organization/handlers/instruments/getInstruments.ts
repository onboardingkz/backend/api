import { Request, Response } from "express";
import { messages } from "../../../../core";
import { authenticate } from "../../../session/services";
import { getIfMember } from "../../services";
import { model as modelInstrument } from "../../../instrument/model";

export const getInstruments = async (req: Request, res: Response) => {
    const params = req.params;
    const user = await authenticate(req);
    const organization = await getIfMember(user, null, params.id);
    if (user && organization) {
        const ids = organization.toObject().instruments.map((n: { _id: string }) => n._id);
        const response = await modelInstrument.find({ _id: { $in: ids } }).populate({
            path: 'accessRequests',
            populate: {
                path: 'from'
            }
        });
        return res.status(200).send(response || []);
    } else {
        return res.status(401).send({
            message: messages.Errors.UNAUTHORIZED
        });
    }
};