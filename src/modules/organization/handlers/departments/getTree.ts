import { Request, Response } from "express";
import { model as modelDepartment, Resource } from "../../../department/model";
import { messages } from "../../../../core";
import { authenticate } from "../../../session/services";
import { getIfMember } from "../../services";
import { selectors } from "../../../department";
import _ from 'lodash';

export const getTree = async (req: Request, res: Response) => {
    const params = req.params;
    const user = await authenticate(req);
    const organization = await getIfMember(user, null, params.id);
    if (user && organization) {
        const departments = await modelDepartment.find({ organization: organization.id, parent: null }).populate(selectors.populator);

        const omitChildrenIfEmpty = (item: { children: ReadonlyArray<unknown> }) => {
            return !item.children.length ? _.omit(item, 'children') : item;
        }

        // @ts-ignore
        const recursePopulate = async (item: Document<Resource>) => {
            return item.children?.length ? {
                ...item,
                key: item._id,
                children: await Promise.all(item.children.map(async (n: string) => {
                    const child = await modelDepartment.findById(n).populate(selectors.populator);
                    return recursePopulate(child.toObject());
                }))
            } : omitChildrenIfEmpty({ ...item, key: item._id });
        }
        return res.status(200).send(await Promise.all(departments.map(async (n) => await recursePopulate(n.toObject()))));
    } else {
        return res.status(401).send({
            message: messages.Errors.UNAUTHORIZED
        });
    }
};