import { Request, Response } from "express";
import { messages } from "../../../core";
import { model as modelUser } from "../../user/model";
import { authenticate } from "../../session/services";
import { getIfOwn } from "../services";
import { transferOwnership as transferOwnershipService } from "../../shared/services";

export const transferOwnership = async (req: Request, res: Response) => {
    const input = req.body;
    const params = req.params;
    const user = await authenticate(req);
    const organization = await getIfOwn(user, null, params.id);
    if (user && organization) {
        const member = await modelUser.findById(input.userId);

        if (member) {
            await transferOwnershipService(organization.id, user.id, member.id);
            return res.status(200).send({
                message: messages.Success.Default
            });
        } else {
            return res.status(400).send({
                message: messages.Errors.WRONG_INPUT
            });
        }
    } else {
        return res.status(401).send({
            message: messages.Errors.UNAUTHORIZED
        });
    }
};