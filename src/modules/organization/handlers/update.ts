import { Request, Response } from "express";
import { model } from "../model";
import { messages } from "../../../core";
import { authenticate } from "../../session/services";
import _ from "lodash";
import {getIfOwn} from "../services";
import {cdn} from "../../../core/api";

export const update = async (req: Request, res: Response) => {
    const input = req.body;
    const params = req.params;
    const user = await authenticate(req);
    const organization = await getIfOwn(user, null, params.id);
    if (user && organization) {
        const updatedOrganization = await model.findByIdAndUpdate(organization._id, { $set: {
                ...(input.name ? { name: input.name } : {}),
                ...(input.bin ? { bin: input.bin } : {}),
                ...(input.address ? { address: input.address } : {}),
                ...(input.phone ? { phone: input.phone } : {}),
                ...(input.email ? { email: input.email } : {}),
                ...(input.avatar ? { avatar: input.avatar } : {}),
            }}, { new: true });
        if (input.avatar && organization.avatar) {
            try {
                await cdn.delete(encodeURI(`remove?file=${organization.avatar}`));
            } catch (err) {
                console.log(err);
            }
        }
        return res.status(200).send(_.omit(updatedOrganization.toObject(), []));
    } else {
        return res.status(401).send({
            message: messages.Errors.UNAUTHORIZED
        });
    }
};