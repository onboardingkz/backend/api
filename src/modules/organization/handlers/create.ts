import { Request, Response } from "express";
import { build } from "../model";
import { messages } from "../../../core";
import { authenticate } from "../../session/services";

export const create = async (req: Request, res: Response) => {
    const input = req.body;
    const user = await authenticate(req);
    if (user) {
        if (input?.name && input?.bin) {
            const organization = build({ 
                name: input.name, 
                bin: input.bin, 
                admin: user.id, 
                address: input.address, 
                phone: input.phone, 
                email: input.email
            });
            await organization.save();
            return res.status(200).send(organization);
        } else {
            return res.status(400).send({
                message: messages.Errors.WRONG_INPUT
            });
        }
    } else {
        return res.status(401).send({
            message: messages.Errors.UNAUTHORIZED
        });
    }
};
