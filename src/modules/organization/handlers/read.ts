import { Request, Response } from "express";
import { model } from "../model";
import { messages } from "../../../core";
import { authenticate } from "../../session/services";
import _ from "lodash";
import { selectors } from "../";

export const read = async (req: Request, res: Response) => {
    const params = req.params;
    const user = await authenticate(req);
    if (params?.id) {
        const organization = await model.findById(params.id).populate(selectors.populator);
        if (organization) {
            return res.status(200).send(
                user ? organization : _.pick(
                    organization.toObject(),
                    selectors.secureFields
                ));
        } else {
            return res.status(404).send({
                message: messages.Errors.NOT_FOUND
            });
        }
    } else {
        return res.status(400).send({
            message: messages.Errors.WRONG_INPUT
        });
    }
};