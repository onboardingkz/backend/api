export const secureFields = [
    'id',
    'name',
    'bin',
    'address',
    'email',
    'phone',
];