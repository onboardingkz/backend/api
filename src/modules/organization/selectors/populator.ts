export const populator = [
    'members',
    'rules',
    'departments',
    'documents',
    'signedDocuments',
    'instruments',
];