import { model } from "../model";
import { Resource as UserResource } from "../../user/model";
import { checkObjectId } from "../../../core/database";
import { selectors } from '../';
import {Request} from "express";

export const getIfMember = async (user: UserResource, req?: Request | null, idExplicitly?: string | null) => {
    const idTmp = req?.headers.organizationid;
    const idHeader = idTmp ? (typeof idTmp === 'string' ? idTmp : idTmp[0]) : null;
    const id = idExplicitly ?? idHeader;
    if (checkObjectId(id)) {
        // @ts-ignore
        return user?.organizations?.includes(id) ? await model.findById(id).populate(selectors.populator) : null;
    } else return null;
}