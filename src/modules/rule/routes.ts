import { Express } from "express";
import { handlers } from ".";
import { generateRoute } from "../../core/router";

export const routes = (app: Express) => {
    app.post(generateRoute('/rule'), handlers.create);
    app.delete(generateRoute('/rule/:id'), handlers.remove);
    app.put(generateRoute('/rule/:id'), handlers.update);
}