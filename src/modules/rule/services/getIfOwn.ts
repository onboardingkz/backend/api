import { model } from "../model";
import { checkObjectId } from "../../../core/database";
import { Resource as UserResource } from "../../user/model";

export const getIfOwn = async (user: UserResource, id: string) => {
    if (checkObjectId(id)) {
        const rule = await model.findById(id);
        return user?.ownedOrganizations?.includes(rule?.organization) ? rule : null;
    } else return null;
}