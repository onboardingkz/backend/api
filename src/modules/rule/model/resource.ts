import * as mongoose from "mongoose";

export type Resource = {
    readonly title: string;
    readonly organization: mongoose.Types.ObjectId;
}
