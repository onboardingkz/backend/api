import mongoose from "mongoose";
import { middlewares } from "./middlewares";

const schema = new mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    organization: {
        type: mongoose.Types.ObjectId,
        ref: "Organization",
        required: true
    },
});

middlewares(schema);

export const model = mongoose.model('Rule', schema);

