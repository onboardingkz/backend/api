import { Request, Response } from "express";
import { build } from "../model";
import { messages } from "../../../core";
import { authenticate } from "../../session/services";
import { getIfOwn } from "../../organization/services";
import { model as modelOrganization } from "../../organization/model";

export const create = async (req: Request, res: Response) => {
    const input = req.body;
    const user = await authenticate(req);
    const organization = await getIfOwn(user, req);
    if (user && organization) {
        if (input?.title) {
            const rule = build({ title: input.title, organization: organization._id });
            rule.save(async (err) => {
                if (err) {
                    return res.status(500).send({
                        message: messages.Errors.SERVER_ERROR
                    });
                } else {
                    await modelOrganization.findByIdAndUpdate(organization._id, { $addToSet: { rules: rule._id } });
                    return res.status(200).send(rule);
                }
            });
        } else {
            return res.status(400).send({
                message: messages.Errors.WRONG_INPUT
            });
        }
    } else {
        return res.status(401).send({
            message: messages.Errors.UNAUTHORIZED
        });
    }
};