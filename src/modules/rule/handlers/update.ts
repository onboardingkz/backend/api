import { Request, Response } from "express";
import { model } from "../model";
import { messages } from "../../../core";
import { authenticate } from "../../session/services";
import { getIfOwn } from "../services";

export const update = async (req: Request, res: Response) => {
    const input = req.body;
    const params = req.params;
    const user = await authenticate(req);
    const rule = await getIfOwn(user, params.id);
    if (user && rule) {
        const updatedRule = await model.findByIdAndUpdate(params.id, { $set: { title: input.title }}, { new: true });
        return res.status(200).send(updatedRule);
    } else {
        return res.status(401).send({
            message: messages.Errors.UNAUTHORIZED
        });
    }
};