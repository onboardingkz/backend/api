import { Request, Response } from "express";
import { model } from "../model";
import { model as modelUser } from "../../user/model";
import { messages } from "../../../core";

export const confirm = async (req: Request, res: Response) => {
    const input = req.body;
    if (input.token) {
        const emailConfirm = await model.findOne({ token: input.token }).exec();
        if (emailConfirm) {
            await modelUser.findByIdAndUpdate(emailConfirm.user, { $set: { confirmed: true }});
            await model.findByIdAndDelete(emailConfirm.id);
            return res.status(200).send({
                message: messages.Success.Default
            });
        } else {
            return res.status(401).send({
                message: messages.Errors.UNAUTHORIZED
            });
        }
    } else {
        return res.status(400).send({
            message: messages.Errors.WRONG_INPUT
        });
    }
};