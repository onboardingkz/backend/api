import { Request, Response } from "express";
import {build, model} from "../model";
import { model as modelUser } from "../../user/model";
import { messages } from "../../../core";
import {authenticate} from "../../session/services";
import {generateToken} from "../../../core/utils";
import {MailerType} from "../../../core/mail";
import {sendConfirmation} from "../services";

export const resend = (mailer: MailerType) => async (req: Request, res: Response) => {
    const input = req.body;
    const user = await authenticate(req);
    if (user) {
        const emailConfirm = await model.findOne({ user: user.id }).exec();
        if (emailConfirm) {
            const differenceSeconds = (Date.now() - emailConfirm.createdAt) / 1000;
            if (differenceSeconds > 60) {
                await model.findByIdAndDelete(emailConfirm.id);
                await sendConfirmation(user, mailer);

                return res.status(200).send({
                    message: messages.Success.Default
                });
            } else {
                return res.status(405).send({
                   message: 'wait'
                });
            }
        } else {
            return res.status(400).send({
                message: messages.Errors.NOT_FOUND
            });
        }
    } else {
        return res.status(401).send({
            message: messages.Errors.UNAUTHORIZED
        });
    }
};