import {generateToken} from "../../../core/utils";
import {build} from "../model";
import {MailerType} from "../../../core/mail";
import { Resource } from "../../user/model";
import {emailConfirmation} from "../../../core/templates";

export const sendConfirmation = async (user: Partial<Resource>, mailer: MailerType) => {
    const emailCode = generateToken(30);
    // @ts-ignore
    await build({ token: emailCode, user: user.id }).save();
    const confirmationLink = `${process.env.FRONTEND_HOSTNAME}/email/confirm?token=${emailCode}`;
    await mailer?.getTransporter.sendMail(
        mailer.buildMessage(
            user.email,
            "Welcome to OnBoarding",
            emailConfirmation(confirmationLink)
        ));
}