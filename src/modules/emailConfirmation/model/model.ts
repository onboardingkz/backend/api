import mongoose from "mongoose";

const schema = new mongoose.Schema({
    token: {
        type: String,
        required: true
    },
    user: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: "User",
    },
}, {
    timestamps: true
});

export const model = mongoose.model('EmailConfirmation', schema);

