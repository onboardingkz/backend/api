import * as mongoose from "mongoose";

export type Resource = {
    readonly token: string;
    readonly user: mongoose.Schema.Types.ObjectId;
}
