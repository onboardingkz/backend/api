import { Express } from "express";
import { handlers } from ".";
import {generateRoute} from "../../core/router";
import {MailerType} from "../../core/mail";

export const routes = (app: Express, mailer: MailerType) => {
    app.post(generateRoute('/email/confirm'), handlers.confirm);
    app.post(generateRoute('/email/resend'), handlers.resend(mailer));
}