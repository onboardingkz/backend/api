import { model as modelUser } from '../../../user/model';
import { model as modelOrganization } from '../../../organization/model';

export const addOwnerToOrganization = async (organizationId: string, userId: string) => {
    await modelUser.findByIdAndUpdate(userId, { $addToSet: { organizations: organizationId }});
    await modelUser.findByIdAndUpdate(userId, { $addToSet: { ownedOrganizations: organizationId }});
    await modelOrganization.findByIdAndUpdate(organizationId, { $addToSet: { members: userId }});
}