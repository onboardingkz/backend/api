import {model as modelUser} from "../../../user/model";
import {model as modelOrganization} from "../../../organization/model";

export const removeMemberFromOrganization = async (organizationId: string, userId: string) => {
    await modelUser.findByIdAndUpdate(userId, { $pull: { organizations: organizationId }});
    await modelOrganization.findByIdAndUpdate(organizationId, { $pull: { members: userId }});
}