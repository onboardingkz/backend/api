import { model as modelUser } from '../../../user/model';
import { model } from '../../../organization/model';

export const transferOwnership = async (organizationId: string, userId: string, newUserId: string) => {
    await model.findByIdAndUpdate(organizationId, { $set: { admin: newUserId } }).exec();
    // Delete organization from owned organizations of previous admin
    await modelUser.findByIdAndUpdate(userId, { $pull: { ownedOrganizations: organizationId }}).exec();

    // Add organization to owned organizations of new admin
    await modelUser.findByIdAndUpdate(newUserId, { $addToSet: { ownedOrganizations: organizationId }}).exec();
}