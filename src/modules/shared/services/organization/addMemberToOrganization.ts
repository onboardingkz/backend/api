import { model as modelUser } from '../../../user/model';
import { model as modelOrganization } from '../../../organization/model';

export const addMemberToOrganization = async (organizationId: string, userId: string) => {
    await modelUser.findByIdAndUpdate(userId, { $addToSet: { organizations: organizationId }});
    await modelOrganization.findByIdAndUpdate(organizationId, { $addToSet: { members: userId }});
}