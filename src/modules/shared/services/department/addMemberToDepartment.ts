import { model as modelUser } from '../../../user/model';
import { model as modelDepartment } from '../../../department/model';

export const addMemberToDepartment = async (departmentId: string, userId: string) => {
    await modelUser.findByIdAndUpdate(userId, { $addToSet: { departments: departmentId }});
    await modelDepartment.findByIdAndUpdate(departmentId, { $addToSet: { members: userId }});
}