import { model as modelUser } from '../../../user/model';
import { model as modelDepartment } from '../../../department/model';

export const removeMemberFromDepartment = async (departmentId: string, userId: string) => {
    await modelUser.findByIdAndUpdate(userId, { $pull: { departments: departmentId }});
    await modelDepartment.findByIdAndUpdate(departmentId, { $pull: { members: userId }});
}