import {build} from "../../modules/organization/model";
import {testOrganization} from "./mocks/testOrganization";

export const initializeExistingOrganism = (user: { id?: string }) => {
    const organization = build(testOrganization(user.id));
    return organization.save();
}