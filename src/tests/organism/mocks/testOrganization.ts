import { Resource } from "../../../modules/organization/model";
import mongoose from "mongoose";

export const testOrganization = (adminId: string): Resource => ({
    name: "Google LLC.",
    bin: 9301293213,
    admin: new mongoose.Schema.Types.ObjectId(adminId)
});