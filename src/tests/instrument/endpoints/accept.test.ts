import { getApp } from "../../server";
import { generateRoute } from "../../../core/router";
import { messages } from '../../../core';
import { setup } from "../../setup";
import { initializeExistingUser } from "../../user/utils";
import {initializeExistingOrganism} from "../../organism/utils";

setup('user', {
    beforeAll: async () => {
        initializeExistingUser()
            .then(async (doc) => {
                await initializeExistingOrganism(doc.toObject());
            });
    }
});

describe("Access Request - ACCEPT", () => {
    test("Successful payload", async () => {
        expect(200).toBe(200);
    });
});
