import { server } from "../server";
import dotenv from "dotenv";
import supertest from "supertest";

dotenv.config();

export const getApp = async () => {
    const appInstance = await server({
        silent: true
    });
    return supertest(appInstance);
};