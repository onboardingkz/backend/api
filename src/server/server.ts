import express from "express";
import { json, urlencoded } from "body-parser";
import { user, organization, rule, notification, emailConfirmation, department, instrument, accessRequest, document, signedDocument } from "../modules";
import { logger } from "../core/config";
import winston from "winston";
import cors from "cors";
import { Config } from "./type";
import { pipeline, statusLog } from "../core/utils";

export const server = async ({ databaseConnection, smtpConnection, socket, silent }: Config) => {
    // port is now available to the Node.js runtime
    // as if it were an environment variable
    const app = express();

    app.use(json());
    app.use(cors());

    // Routes
    user.routes(app, smtpConnection);
    organization.routes(app);
    rule.routes(app);
    notification.routes(app, socket);
    emailConfirmation.routes(app, smtpConnection);
    department.routes(app);
    instrument.routes(app);
    accessRequest.routes(app, socket);
    document.routes(app);
    signedDocument.routes(app);

    if (process.env.NODE_ENV !== 'production') {
        logger.add(new winston.transports.Console({
            format: winston.format.simple(),
        }));
    }

    if (!silent) {
        await pipeline(
            // Check database connection
            (next) => {
                databaseConnection
                    .then(() => {
                        statusLog('success', 'Database successfully connected');
                    })
                    .catch(() => {
                        statusLog('error', 'Database error! Check if your MongoDB server is running');
                        statusLog('error', process.env.DATABASE_CONNECTION_STRING)
                        process.exit(0);
                    })
                    .finally(() => next(null));
            },
            // Check mail service
            (next) => {
                if (smtpConnection) {
                    smtpConnection.checkConnection()
                        .then(() => {
                            statusLog('success', 'Mail server successfully connected');
                        })
                        .catch(() => {
                            statusLog('warn', 'Mail service is not available! Launching with mail service off');
                        })
                        .finally(() => next(null));
                } else next(null);
            }
        )
            .then(() => void 0);
    }

    return app;
}


