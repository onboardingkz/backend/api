import mongoose from "mongoose";
import { ReturnType } from "../core/mail/types";
import { Socket } from "socket.io-client";

export type Config = {
    databaseConnection?: Promise<typeof mongoose>,
    smtpConnection?: ReturnType,
    silent?: boolean,
    socket?: Socket
}