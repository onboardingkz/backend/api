export enum Errors {
    WRONG_INPUT = "Ошибка! Заполните все поля",
    SERVER_ERROR = "Ошибка сервера. Попробуйте обновить страницу",
    ALREADY_EXISTS = "Ошибка! Данные уже существуют",
    WRONG_CREDENTIALS = "Ошибка! Введена неправильная почта или пароль",
    UNAUTHORIZED = "unauthorized",
    NOT_FOUND = "Не найдено"
}