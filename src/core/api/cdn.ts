import axios from "axios";
import dotenv from 'dotenv';

dotenv.config();

export const cdn = axios.create({
    baseURL: process.env.CDN_URL,
    headers: {
        "Authorization": `Bearer ${process.env.CDN_TOKEN}`
    }
});