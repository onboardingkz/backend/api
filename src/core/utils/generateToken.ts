export const generateToken = (length: number) => {
    const symbols = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".split("");
    const tmpArr = [];
    for (let i=0; i<length; i++) {
        const j = Number((Math.random() * (symbols.length-1)).toFixed(0));
        tmpArr[i] = symbols[j];
    }
    return tmpArr.join("");
};