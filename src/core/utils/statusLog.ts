enum statuses { success, warn, error, finished }

const statusesEmoji: Record<keyof typeof statuses, string> = {
    success: "\x1b[32m✔\x1b[0m",
    finished: "\x1b[32m✔✔✔\x1b[0m",
    warn: "\x1b[33m!\x1b[0m",
    error: "\x1b[31mX\x1b[0m",
}

export const statusLog = (status: keyof typeof statuses, ...body: ReadonlyArray<string>) => {
    console.log(statusesEmoji[status], ...body);
}