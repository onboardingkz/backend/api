import winston from "winston";
const { format: { timestamp, prettyPrint, json } } = winston;

export const logger = winston.createLogger({
    level: 'info',
    format: winston.format.combine(
        json(),
        prettyPrint(),
    ),
    transports: [
        new winston.transports.File({ filename: 'error.log', level: 'error' }),
        new winston.transports.File({ filename: 'combined.log' }),
    ],
});