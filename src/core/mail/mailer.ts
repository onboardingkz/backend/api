import { createTransport } from "nodemailer";
import { ArgsType, ReturnType } from "./types";

export const mailer = ({ host, port, auth } : ArgsType): ReturnType => {
    const transport = createTransport({
        host,
        port,
        secure: false, // upgrade later with STARTTLS
        auth: {
            user: auth.email,
            pass: auth.password,
        },
    });

    const checkConnection = () => {
        return new Promise((resolve, reject) => {
            transport.verify( (error, success) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(success);
                }
            });
        });
    }

    const buildMessage = (to: string, subject: string, html: string) => ({
        from: auth.email,
        to,
        subject,
        html,
    });

    return {
        getTransporter: transport,
        buildMessage,
        checkConnection
    };
}