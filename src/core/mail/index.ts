import { ArgsType, ReturnType } from "./types";

export * from './mailer';

export type MailerArgsType = ArgsType;
export type MailerType = ReturnType;