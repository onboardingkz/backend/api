import { Transporter } from "nodemailer";

export type ArgsType = {
    readonly host: string;
    readonly port: number;
    readonly auth: {
        readonly email: string;
        readonly password: string;
    }
}

export type ReturnType = {
    readonly checkConnection: () => Promise<unknown>;
    readonly getTransporter: Transporter;
    readonly buildMessage: (to: string, subject: string, html: string) => { from: string, to: string, subject: string; html: string; }
};